package java.lang.annotation;

@Documented
@Retention(value= RetentionPolicy.RUNTIME)
@Target(value=ElementType.TYPE)
public @interface FunctionalInterface {}